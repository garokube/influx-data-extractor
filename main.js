const express = require('express');
const app = express();
const port = 3000;
const Promise = require("bluebird");

const Influx = require('influx');

const databases = {
  'openhab' : new Influx.InfluxDB('http://admin:admin@influxdb.infra.svc.cluster.local:8086/openhab'),
  'data' : new Influx.InfluxDB('http://admin:admin@influxdb.infra.svc.cluster.local:8086/data')
};

const Prometheus = require('prom-client');
Prometheus.collectDefaultMetrics({timeout: 5000});

const iot_temperature = new Prometheus.Gauge({
  name: 'iot_temperature',
  help: 'general iot temperature measurement',
  labelNames: ['name']
});

const queries = [
  {
    query: 'SELECT last("value") as value FROM "Nibe_BT1_Outdoor_Temp"',
    database: 'openhab',
    prometheus_item: iot_temperature,
    prometheus_labels: ['Nibe_BT1_Outdoor_Temp']
  }
];



console.log("influx-to-prometheus starting with", queries.length, "queries");
setInterval(() => {
  Promise.each(queries, async op => {

    try {
      const influx = databases[op.database];
      const rows = await influx.query(op.query);
      if (rows.length == 1) {
        const row = rows[0];
        const accepted_measurement_age_in_seconds = 8 * 60;
        const cutoff = ((new Date()).getTime() - accepted_measurement_age_in_seconds * 1000) * 1000000;
        if (row.time.getNanoTime() < cutoff) {
          console.log("Last time is too far in the past. will not accept", (row.time.getNanoTime() - ((new Date()).getTime() * 1000000)) / 1000000000);
        } else {
          const value = row.value;
          op.prometheus_item.labels.apply(op.prometheus_item, op.prometheus_labels).set(value);
        }
      }
      else {
        console.log("Got weird number of rows:", rows.length, "from query", query);
      }
    }
    catch (error) {
      return console.error(error);
    }

  });
}, 15000);



app.get('/metrics', function(req, res) {
  res.type(Prometheus.register.contentType);
  res.send(Prometheus.register.metrics());
});
app.listen(port, () => console.log(`Example app listening on port ${port}!`))



